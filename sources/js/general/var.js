/* GENERAL */

const changeFunctions = ( newName, oldName ) => {
    [Window, Document, Element].map( selector => selector.prototype[`${newName}`] = selector.prototype[`${oldName}`] )
}


const win = window,
    doc = document,
    addEv = changeFunctions('addEv', 'addEventListener'),
    que = changeFunctions('que', 'querySelector'),
    queAll = changeFunctions('queAll', 'querySelectorAll'),
    gId = changeFunctions('gId', 'getElementById')


export { win, doc, addEv, que, queAll, gId }