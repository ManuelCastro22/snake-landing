// IMPORTS
import {
	doc,
	addEv,
	que,
	queAll
} from "../general/var"



export const menu = () => {
    const hamBtn = doc.queAll('.hamburgerBtn')

	hamBtn.length > 0 && [...hamBtn].map( btn => {
		btn.addEv('click', e => {
			e.preventDefault()
			
			e.currentTarget.classList.toggle('active')
			doc.que('.nav--main').classList.toggle('active')
		})
	} )
}