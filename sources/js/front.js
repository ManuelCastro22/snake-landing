/* FRONT */

// IMPORTS
import {
	win,
	doc,
	que
} from "./general/var.js"


// IMPORT COMPONENTS OR CLASSES
import { openLoader } from './components/loader.js'
import { Gallery } from './class/Gallery.js'
import { menu } from './components/menu.js'
import { modal } from './components/modal.js'



/* ON LOAD */
win.onload = () => doc.que('.loader').classList.add('hide')



/* LOAD COMPLETE */
doc.addEv('DOMContentLoaded', () => {


	// OPEN LOADER
	openLoader()


	// GALLERY
	const containerShuffle = doc.que('.gallery__container'),
		gallery = containerShuffle && new Gallery('.gallery__container')
	
	const galleryShowMore = containerShuffle.closest('.gallery').que('.gallery__showMore')

	galleryShowMore && galleryShowMore.addEv('click', e => {
		e.preventDefault()

		gallery.increaseGallery()
	})
	
	// MENU
	menu()


	// MODAL
	modal()

	
})