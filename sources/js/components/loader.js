/* OPEN LOADER */

// IMPORTS
import {
    doc,
    addEv,
    que
} from "../general/var.js"



export const openLoader = () => {
    const btnsLoader = doc.queAll(`.oLoader`)

    btnsLoader.length > 0 && [...btnsLoader].map( btnLoader => {
        btnLoader.addEv('click', () => doc.que('.loader').classList.remove('hide') )
    } )
}