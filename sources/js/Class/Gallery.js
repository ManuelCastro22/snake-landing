/* CLASS FOR GALLERY */

// IMPORTS
import {
    doc,
    que,
    queAll,
    addEv
} from "../general/var.js"
import Shuffle from 'shufflejs'
import { Data } from './Data.js' 


export class Gallery {
    constructor( container , {shuffle = true} = {} ){
        this.data = new Data(),
        this.galleryItems = [],
        this.shuffle = shuffle,
        this.container = container,
        this.parentGallery = doc.que(this.container).closest('.gallery'),
        this.toShow = 10,
        this.init()
    }

    init(){
        this.showGallery()
    }

    showGallery(){
        this.data.getData('./json/data.json')
            .then( res => {
                this.galleryItems = [...res];
                [...res].filter( (dataGallery, index) => {
                    if( index < this.toShow ) {
                        const { title, categories, nameImg, url } = dataGallery
                        this.printGallery(title, categories, nameImg, url)
                    }
                } )
            } )
            .then( () => this.shuffle && this.printShuffle() )
            .catch( error => console.log(error) )
    }

    clearGallery() {
        doc.que(this.container).innerHTML = ''
    }

    printGallery(title, categories, nameImg, url){

        const categoriesQuotes = categories.map( category => `"${category}"` ).toString()

        const template = `
            <!-- GALLERY ITEM -->
            <a href="${url}" class="gallery__item" data-groups='["all", ${categoriesQuotes} ]'>
                <img src="img/gallery/${nameImg}" alt="" class="gallery__item__bg">
                <div class="gallery__item__content">
                    <h2 class="gallery__item__title">${title}</h2>
                    <hr class="gallery__item__divider">
                    <h3 class="gallery__item__categories">${ categories.join(", ") }</h3>
                </div>
            </a>
            <!-- END GALLERY ITEM -->        
        `
        doc.que(this.container).insertAdjacentHTML('beforeend', template)
    }

    printShuffle(){
        const shuffleInstance = new Shuffle(doc.que(this.container), {
            itemSelector: '.gallery__item'
        })

        this.orderShuffle(shuffleInstance)
        this.filterShuffle(shuffleInstance)
    }

    filterShuffle(instance){
        const galleryNavLinks = doc.queAll('.gallery__nav .nav__list__link')

        galleryNavLinks.length > 0 && [...galleryNavLinks].map( btnGN => {
            btnGN.addEv('click', e => {
                e.preventDefault()

                doc.que('.gallery__nav .nav__list__link.active').classList.remove('active')
                btnGN.classList.add('active')

                const keyword = btnGN.dataset.target
                instance.filter(keyword)
            })
        } )
    }

    orderShuffle(instance){
        const gallery__order__btn = doc.queAll('.gallery__order__btn')

        gallery__order__btn.length > 0 && [...gallery__order__btn].map( btnGO => {
            btnGO.addEv( 'click', e => {
                e.preventDefault()

                doc.que('.gallery__order__btn.active').classList.remove('active')
                btnGO.classList.add('active');

                [...doc.queAll('.gallery__item')].map( galleryItem => {
                    galleryItem.classList[ btnGO.classList.contains('gallery__order__btn--square') ? 'remove' : 'add' ]('w100')
                } )

                setTimeout( () => instance.update(), 500 )

            } )
        } )
    }

    increaseGallery() {
        this.toShow = this.toShow + 10
        if( this.toShow >= this.galleryItems.length ) this.hideShowMore()
        this.clearGallery()
        this.showGallery()
        this.restartGallery()
    }

    restartGallery() {
        const oldFilterActive = this.parentGallery.que('.nav__list__link.active'),
            newFilterActive = this.parentGallery.que('.nav__list__link[data-target="all"]'),
            oldOrderActive = this.parentGallery.que('.gallery__order__btn.active'),
            newdOrderActive = this.parentGallery.que('.gallery__order__btn--square')

        oldFilterActive.classList.remove('active')
        oldOrderActive.classList.remove('active')
        newFilterActive.classList.add('active')
        newdOrderActive.classList.add('active')
    }

    hideShowMore() {
        this.parentGallery.que('.gallery__showMore').classList.add('hidden')
    }

}