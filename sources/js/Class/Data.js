/* DATA JSON */

export class Data {
    async getData(url){
        const queryData = await fetch(url),
            dataJSON = await queryData.json()

        return dataJSON
    }
}